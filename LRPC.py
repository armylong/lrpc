import hprose
from Zk import Zk

class LRPC(object):

    zk_host = "10.10.10.100:2181"  # soyoung本地
    rpc_zk_dir = "/server_list"
    client = None
    server_name = None
    space_name = None
    function_name = None
    return_data = None

    def __init__(self, server_name, space_name):
        self.space_name = space_name
        hosts = self.getHost(server_name)
        self.client = hprose.HttpClient(hosts)

    """
    服务调用
    """
    def __getattr__(self, function_name):
        def _func(*args):
            exec("self.return_data = self.client.{}.{}(*args)".format(self.space_name, function_name))
            return self.return_data

        setattr(self, function_name, _func)
        return _func

    """
    服务发现
    """
    def getHost(self, server_name):
        node_path = self.rpc_zk_dir + "/" + server_name
        zk = Zk(self.zk_host)
        hosts = zk.getNodeValue(node_path)
        zk.zk.stop()
        return hosts



if __name__ == '__main__':
    doctor_info = LRPC("py_soyoung_server", "Doctor").getDoctorInfo(148664)
    print(doctor_info)

    es_data = LRPC("py_soyoung_server", "Tools").es_select("ade_doctor_special_info")
    print(es_data)

