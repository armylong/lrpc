import json
import time
from kazoo.client import KazooClient

class Zk(object):

    watch_node = ""

    def __init__(self, hosts='101.34.45.186:2181'):
        self.zk = KazooClient(hosts=hosts)  # 连接zk
        self.zk.start()  # 与zookeeper连接
        # print("连接zk")
        node = self.zk.get_children('/')
        # print(node)

    def __del__(self):
        # print("与zookeeper断开")
        self.zk.stop()  # 与zookeeper断开

    # 节点预览
    def dirView(self, nodePath="/"):
        # node_list = self.getChildrenNodeList()
        node_list = self.zk.get_children(nodePath)
        for node in node_list:
            if nodePath == "/":
                print("\n")
                node = nodePath + node
            else:
                node = nodePath + "/" + node
            print(node)
            self.getNodeValue(node)
            self.dirView(node)


    # 建立节点，成功后返回新节点路径
    def createNode(self, nodePath, value_py, tmp_node = False):
        """
        path：          节点路径
        value：         节点对应的值，注意值的类型是 bytes
        ephemeral： 若为 True 则创建一个临时节点，session 中断后自动删除该节点。默认 False
        sequence:     若为 True 则在你创建节点名后面增加10位数字（例如：你创建一个 testplatform/test 节点，实际创建的是 testplatform/test0000000003，这串数字是顺序递增的）。默认 False
        makepath：  若为 False 父节点不存在时抛 NoNodeError。若为 True 父节点不存在则创建父节点。默认 False
        """
        try:
            value_b = self.py2b(value_py)
            if not self.existsNode(nodePath):
                if not tmp_node:
                    newPath = self.zk.create(nodePath, value_b, makepath=True)
                else:
                    newPath = self.zk.create(nodePath, value_b, makepath=True, ephemeral=True)
                print(newPath, "zk节点创建成功")
                return True
            else:
                print(nodePath, "无需重复创建")
                return True
        except Exception as e:
            print(e)
            return False

    # 获取子节点列表你
    def getChildrenNodeList(self, nodePath):
        if self.existsNode(nodePath):
            node = self.zk.get_children(nodePath)
            print(node)
            return node
        else:
            return []

    # 获取节点的值
    def getNodeValue(self, nodePath):
        node_value = self.zk.get(nodePath)
        node_value_b = node_value[0]
        value_py = self.b2py(node_value_b)
        # print(value_py)
        return value_py

    # 获取根目录下节点列表你
    def getRootNodeList(self):
        node_list = self.zk.get_children('/')  # 查看根节点有多少个子节点
        print(node_list)
        return node_list

    # 设置节点数据
    def setNodeValue(self, nodePath, value_py):
        value_b = self.py2b(value_py)
        self.zk.set(nodePath, value_b)
        print(nodePath, "修改完成")

    # 删除节点
    def delNode(self, nodePath, recursive=True):
        self.zk.delete(nodePath, recursive=recursive)  # recursive=True代表默认删除子节点
        print(nodePath, "删除成功")
        return True

    # 判断节点是否存在
    def existsNode(self, nodePath):
        if self.zk.exists(nodePath):
            # print(nodePath, "zk节点存在")
            return True
        else:
            # print(nodePath, "zk节点不存在")
            return False

    def watchNode(self, nodePath):
        while True:
            time.sleep(1)
            self.watch_node = nodePath
            self.zk.get(nodePath, watch=self.watchCallBack)

    def watchCallBack(self, event):
        print("节点数据有变化")
        self.getNodeValue(self.watch_node)
        print(event)
        pass

    def b2py(self, value_b):
        if value_b:
            value_json = value_b.decode()
            if value_json:
                if type(value_json) != str:
                    return json.loads(value_json)
                else:
                    return json.loads(value_json)
            else:
                return ""
        else:
            return ""

    def py2b(self, value_py):
        value_json = json.dumps(value_py)
        return value_json.encode()

if __name__ == '__main__':
    nodePath = "/server_list/py_doctor_server"
    # value = {"host": "101.34.45.186"}
    value = "http://127.0.0.1:8081"

    # host = "10.10.10.100:2181"  # soyoung本地
    host = "101.34.45.186:2181"  # armylong.com
    zk = Zk(host)
    # zk.dirView()
    # zk.getChildrenNodeList(nodePath)
    # zk.createNode(nodePath, value)
    # zk.setNodeValue(nodePath, value)
    data = zk.getNodeValue(nodePath)
    print(data)
    # zk.delNode(nodePath)
    # zk.watchNode(nodePath)
    zk.zk.stop()


